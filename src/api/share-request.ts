// https://www.linkedin.com/sharing/share-offsite/?url={url}

export const requestShareUrl = async (url: string) => {
    const requestUrl = `https://www.linkedin.com/sharing/share-offsite/?url=${url}`;
    window.open(requestUrl, '_blank');
};
