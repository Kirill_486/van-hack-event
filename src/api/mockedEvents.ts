import {IVanHackEvent} from "../app-domain/Interfaces"

export const eventsDummmieData: IVanHackEvent[] = [
    {
        id: '1',
        title: 'Vestibulum elit velit, volutpat eu tempus faucibus, sodales eget ligula.',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris purus, interdum ut purus ac, convallis scelerisque turpis. Phasellus semper aliquam enim, id cursus mi placerat ac. Maecenas sodales faucibus sapien, a rutrum mauris sollicitudin eget. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras gravida ligula ut justo tempor, in tincidunt tellus elementum. In quis ante a justo auctor varius. Ut a tristique ante, eget blandit turpis. Proin ut urna ut erat euismod rhoncus. Proin a suscipit est. Sed mollis urna accumsan elit tincidunt, at blandit eros gravida. Vivamus ligula libero, pellentesque ut semper ac, commodo ac velit. Sed risus erat, aliquam nec euismod accumsan, auctor consequat velit. Etiam hendrerit lacus blandit, pretium eros id, fringilla purus.',
        isPremium: false,
        eventUrl: 'https://en.wikipedia.org/wiki/Lorem_ipsum',
    },
    {
        id: '2',
        title: 'Donec at libero libero. Donec at dolor tortor.',
        description: 'Praesent finibus eros a lectus consequat tempus. Cras non arcu urna. Suspendisse placerat mi nec urna scelerisque facilisis. Vestibulum id mattis dolor, eget sodales mauris. Cras luctus a enim id imperdiet. Nam quis velit non sapien faucibus tincidunt. Fusce sollicitudin facilisis tellus, non imperdiet lacus blandit ac. Sed pretium metus at libero eleifend porttitor. Maecenas vel lobortis sem. Donec pellentesque purus a aliquet dictum. Donec bibendum urna at enim vestibulum, in euismod lectus feugiat. Nulla vel sem quis risus fringilla placerat viverra consequat velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Proin laoreet metus sed nisl scelerisque semper. Vestibulum vitae ligula massa.',
        isPremium: false,
        eventUrl: 'https://en.wikipedia.org/wiki/Event_horizon',
    },
    {
        id: '3',
        title: 'Morbi et rhoncus libero. Sed egestas vestibulum molestie.',
        description: 'Sed id odio ac sapien lobortis cursus. Proin dictum tellus sed tristique fringilla. Donec vel ex vitae enim dignissim lacinia at id odio. Morbi congue rutrum nibh. Cras ac nulla nulla. Curabitur ut dui fermentum, placerat nisl nec, euismod lectus. Vivamus ac ante sit amet magna consequat sodales eu eget nisi. Quisque porta at ante nec lacinia. Maecenas ut ultrices magna. Sed sem felis, pulvinar nec nulla vitae, pretium maximus nibh. Sed sodales hendrerit tellus, laoreet molestie diam consectetur vitae.',
        isPremium: true,
        eventUrl: 'https://en.wikipedia.org/wiki/Event',
    },
];