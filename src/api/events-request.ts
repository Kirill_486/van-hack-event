import { IVanHackEvent } from "../app-domain/Interfaces";
import { eventsDummmieData } from "./mockedEvents";

// we kind of requesting something;
export const getEventsFromAPI = async (): Promise<IVanHackEvent[]> => {
    return eventsDummmieData;
}