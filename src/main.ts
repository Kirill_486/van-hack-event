import { BuisinessController, changedEventKey } from "./app-domain/BuisinessLogic";

declare const $;

const app = new BuisinessController();

document.addEventListener(changedEventKey, app.rerenderApp);

const PromiseToLoadWindow = new Promise((resolve, reject) => {
    window.addEventListener('load', resolve);
});

const PromiseToGetData = app.getDataFromApi();

Promise.all([PromiseToLoadWindow, PromiseToGetData])
.then(app.fireStateChangedEvent)
.catch((e) => {
    app.showError(e);
    app.fireStateChangedEvent();
});
