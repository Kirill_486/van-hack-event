import { getTemplateById, MessageTemplate, getIdSelector, MessageMessageClass, getClassSelector, MessageErrorClass, MessageBackClass } from "./constants";
import { IMessageScreenViewModel, IView } from "./types";
import { ViewBlueprint } from "./view";

export class Confirmation
extends ViewBlueprint<IMessageScreenViewModel> {
  createDOM({message, backToBrowsing}: IMessageScreenViewModel): IView {
    const element = $(getTemplateById(MessageTemplate));
    $(element).find(getClassSelector([MessageMessageClass])).text(message);
    const backButtonSelector = `button${getClassSelector([MessageBackClass])}`;
    const backButton = $(element).find(backButtonSelector);
    backButton.click(backToBrowsing);
    return {
      instance: element,
      childrenRoot: undefined,
    }
  }
}
