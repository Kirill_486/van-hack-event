import { IView, IViewReadonly } from "./types";

export abstract class ViewBlueprint<ViewModel> implements IViewReadonly {
    private __instance: JQuery<HTMLElement>;
    private __childrenRoot: JQuery<HTMLElement>;

    get instance() {
        return this.__instance;
    }

    get childrenRoot() {
        return this.__childrenRoot;
    }

    constructor(private host: Element, vm: ViewModel) {
        const {instance, childrenRoot} = this.createDOM(vm);
        this.__instance = instance;
        this.__childrenRoot = childrenRoot;
        $(host).append(instance);
    }

    abstract createDOM(vm: ViewModel): IView;
}