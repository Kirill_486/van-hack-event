import { getTemplateById, PremiumTemplate, getClassSelector, PremiumMessage, PremiumBack, PremiumGetPremium } from "./constants";
import { IPremiumLinkViewModel, IView } from "./types";
import { ViewBlueprint } from "./view";

const text = "Here's your premium subscription special offer:"

export class PremiumLink
extends ViewBlueprint<IPremiumLinkViewModel> {
    createDOM({onGetPremiumClick, backToBrowsing}: IPremiumLinkViewModel): IView {

      const element = $(getTemplateById(PremiumTemplate));
      element.find(getClassSelector([PremiumMessage])).text(text);
      element.find(getClassSelector([PremiumGetPremium])).click(onGetPremiumClick);
      element.find(getClassSelector([PremiumBack])).click(backToBrowsing);

      return {
        instance: element,
        childrenRoot: undefined,
      }
    }
}