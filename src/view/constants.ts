export const appRoot = 'application__root';
export const premiumRoot = 'get__premium';

export const SingleEventTemplate = 'event__template';
export const SingleEventTitle = 'event__title';
export const SingleEventDetailsButton = 'event__detailed';
export const SingleEventApplyButton = 'event__apply';
export const SingleEventShareButton = 'event__share';
export const SingleEventPremiumClass = 'event--premium';
export const SingleEventAppliedClass = 'event--applied';

export const SingleEventDetailedTemplate = 'event-detailed__template';
export const SingleEventDetailedTitle = 'event-detailed__title';
export const SingleEventDetailedDescription = 'event-detailed__description';
export const SingleEventDetailedApplyButton = 'event-detailed__apply';
export const SingleEventDetailedShareButton = 'event-detailed__share';
export const SingleEventDetailedBackButton = 'event-detailed__back';
export const SingleEventDetailedPremiumClass = 'event-detailed--premium';
export const SingleEventDetailedAppliedClass = 'event-detailed--applied';


export const EventsListTemplate = 'events-list__template';
export const EventsListInnerRootTemplate = 'events-list__inner';

export const MessageTemplate = 'message__template';
export const MessageMessageClass = 'message__message';
export const MessageBackClass = 'message__back';
export const MessageErrorClass = 'message--error';

export const PremiumTemplate = 'premium__template';
export const PremiumMessage = 'premium__message';
export const PremiumGetPremium = 'premium__get';
export const PremiumBack = 'premium__back';


export const getTemplateById = (id: string) => {
    return $(`#${id}`).html();
}

export const getIdSelector = (elementId: string) => `#${elementId}`;
export const getClassSelector = (elementsClass: string[]) => elementsClass.map((elementClass) => `.${elementClass}`).join();
