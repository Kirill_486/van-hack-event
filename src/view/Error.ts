import { getTemplateById, MessageTemplate, getIdSelector, MessageMessageClass, getClassSelector, MessageErrorClass, MessageBackClass } from "./constants";
import { IMessageScreenViewModel, IView } from "./types";
import { ViewBlueprint } from "./view";

export class ErrorMessage
extends ViewBlueprint<IMessageScreenViewModel> {
  createDOM({message, backToBrowsing}: IMessageScreenViewModel): IView {
    const element = $(getTemplateById(MessageTemplate));
    element.addClass(MessageErrorClass);
    $(element).find(getClassSelector([MessageMessageClass])).text(message);
    $(element).find(getClassSelector([MessageBackClass])).click(backToBrowsing);

    return {
      instance: element,
      childrenRoot: undefined,
    }
  }
}
