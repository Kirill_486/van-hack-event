import { IDetailedEventViewModel, IView } from "./types";
import { SingleEventDetailedTemplate, getTemplateById, getClassSelector, SingleEventDetailedTitle, SingleEventDetailsButton, SingleEventDetailedApplyButton, SingleEventDetailedShareButton, SingleEventDetailedBackButton, SingleEventDetailedPremiumClass, SingleEventDetailedAppliedClass, SingleEventDetailedDescription } from "./constants";
import { ViewBlueprint } from "./view";

export class DetailedEvent
extends ViewBlueprint<IDetailedEventViewModel> {
  createDOM({event, onApplyClick, onShareLinkedInClick, backTOBrowsing}: IDetailedEventViewModel): IView {
    const element = $(getTemplateById(SingleEventDetailedTemplate));

    const {title, applied, isPremium, description} = event;

    element.find(getClassSelector([SingleEventDetailedTitle])).text(title);
    element.find(getClassSelector([SingleEventDetailedDescription])).text(description);
    element.find(getClassSelector([SingleEventDetailedApplyButton])).click(onApplyClick);
    element.find(getClassSelector([SingleEventDetailedShareButton])).click(onShareLinkedInClick);
    element.find(getClassSelector([SingleEventDetailedBackButton])).click(backTOBrowsing);

    if (isPremium) element.addClass(SingleEventDetailedPremiumClass);

    return {
      instance: element,
      childrenRoot: undefined,
    }
  }
}
