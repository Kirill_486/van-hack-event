import { SingleEventTemplate, getTemplateById, getClassSelector, SingleEventPremiumClass, SingleEventTitle, SingleEventDetailsButton, SingleEventApplyButton, SingleEventShareButton, SingleEventAppliedClass } from "./constants";
import { IEventViewModel, IView } from "./types";
import { ViewBlueprint } from "./view";

export class SingleEvent
extends ViewBlueprint<IEventViewModel> {
  createDOM({event, drillDown, onApplyClick, onShareLinkedInClick}: IEventViewModel): IView {
    const {title, applied, isPremium} = event;
    const element = $(getTemplateById(SingleEventTemplate));

    element.find(getClassSelector([SingleEventTitle])).text(title);

    const selector = getClassSelector([SingleEventDetailsButton]);
    const detaidedButton = element.find(getClassSelector([SingleEventDetailsButton]));
    detaidedButton.click(drillDown);

    const applyButton = element.find(getClassSelector([SingleEventApplyButton]));
    applyButton.click(onApplyClick);

    const shareButton  = element.find(getClassSelector([SingleEventShareButton]));
    shareButton.click(onShareLinkedInClick);

    if (applied) element.addClass(SingleEventAppliedClass);
    if (isPremium) element.addClass(SingleEventPremiumClass);

    return {
      instance: element,
      childrenRoot: undefined,
    }
  }
}
