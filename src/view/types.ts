import { IVanHackEvent, command } from "../app-domain/Interfaces";

export interface IMessageScreenViewModel {
    message: string;
    backToBrowsing: command;
}

export interface IEventListViewModel {
    events: IVanHackEvent[];
}

interface ISingleEventViewModel {
    event: IVanHackEvent;

    onApplyClick: command;
    onShareLinkedInClick: command;
}

export interface IDetailedEventViewModel
extends ISingleEventViewModel {
    backTOBrowsing: command;
}

export interface IEventViewModel
extends ISingleEventViewModel {
    drillDown: command;
}

export interface IPremiumLinkViewModel {
    onGetPremiumClick: command;
    backToBrowsing: command;
}

export interface IView {
    instance: JQuery<HTMLElement>;
    childrenRoot?: JQuery<HTMLElement>;
}

export type IViewReadonly = Readonly<IView>;