import { IEventListViewModel, IView } from "./types";
import { getTemplateById, EventsListTemplate, getClassSelector, EventsListInnerRootTemplate, SingleEventTemplate, SingleEventTitle, SingleEventApplyButton, SingleEventDetailsButton, SingleEventShareButton, SingleEventAppliedClass, SingleEventPremiumClass } from "./constants";
import { ViewBlueprint } from './view';

export class EventList
extends ViewBlueprint<IEventListViewModel> {
  createDOM(vm: IEventListViewModel):IView {
    const list = $(getTemplateById(EventsListTemplate));
    const childrenRoot = $(list.find(getClassSelector([EventsListInnerRootTemplate])).get()[0]);

    return {
       instance: list,
       childrenRoot,
    }
  }
}
