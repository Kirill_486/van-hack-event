import { IVanHackEvent } from "../app-domain/Interfaces";

export class EventsModel {
    events: IVanHackEvent[] = [];
}
