import { AppState } from "../app-domain/BuisinessLogic";
import { id } from "../app-domain/Interfaces";

export class ApplicationState {
    selected?: id = undefined;
    state: AppState = AppState.BROWSE;
    message: string;
}
