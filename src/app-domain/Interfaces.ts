export type id = string;
export type command = (...args: any) => void;

export interface IVanHackEvent {
    id: string;
    title: string;
    description: string;
    isPremium: boolean;
    applied?: boolean;

    eventUrl: string;
}

// ALL that could possibly happend in the application
export interface IBuisinessLogic {
    getDataFromApi: () => Promise<void>;

    displayEventDetails: command;
    backToBrowsing: command;

    applyEvent: command;
    shareEvent: command;

    showError: command;
    showConfirmation: command;

    fireStateChangedEvent: command;
    rerenderApp: command;
}
