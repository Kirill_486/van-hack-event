import { getEventsFromAPI } from "../api/events-request";
import { EventsModel } from "../model/events-model"
import { ApplicationState } from "../model/selected-event";
import { requestShareUrl } from "../api/share-request";
import { id, IBuisinessLogic } from "./Interfaces";
import { appRoot, getIdSelector, premiumRoot } from "../view/constants";
import { EventList } from "../view/EventList";
import { IEventListViewModel, IDetailedEventViewModel, IEventViewModel, IMessageScreenViewModel, IPremiumLinkViewModel } from "../view/types";
import { SingleEvent } from "../view/SingleEvent";
import { DetailedEvent } from "../view/DetailedEvent";
import { ErrorMessage } from "../view/Error";
import { Confirmation } from "../view/Confirmation";
import { requestApplyEvent } from "../api/apply-event";
import { PremiumLink } from "../view/PremiumLink";
import { getPremiumNav } from "../api/get-premium";

export class BuisinessController
implements IBuisinessLogic {
    lastCheck: Date;
    eventsModel: EventsModel;
    selected: ApplicationState;

    constructor() {
        this.eventsModel = new EventsModel();
        this.selected = new ApplicationState();

        this.fireStateChangedEvent = this.fireStateChangedEvent.bind(this);
        this.rerenderApp = this.rerenderApp.bind(this);

        this.displayEventDetails = this.displayEventDetails.bind(this);
        this.backToBrowsing = this.backToBrowsing.bind(this);

        this.applyEvent = this.applyEvent.bind(this);
        this.shareEvent = this.shareEvent.bind(this);

        this.showError = this.showError.bind(this);
        this.showConfirmation = this.showConfirmation.bind(this);
    }

    async getDataFromApi() {
        const eventData = await getEventsFromAPI();
        this.eventsModel.events = eventData;
    }

    displayEventDetails(id: id) {
        console.log('displayEventDetails');
        this.selected.state = AppState.BROWSE;
        this.selected.selected = id;
        this.fireStateChangedEvent();
    }

    applyEvent(id?: id) {
        console.log('applyEvent');

        const selectedEventId = id || this.selected.selected;
        const eventIsDefined = typeof selectedEventId !== "undefined";
        if (eventIsDefined) {
            const {eventUrl, isPremium} = this.eventsModel.events.find((event) => event.id ===  selectedEventId);

            if (isPremium) {
                this.showFriendlyError(ThisEventOnlyAvailableForPremiumUsers);
            } else if (eventUrl) {
                requestApplyEvent(eventUrl)
                .then((successs) => {
                    this.showConfirmation(YouSuccessfulyAppliedTheEvent);
                });
            }
        } else {
            this.showError(AttemptToOperateWithUndefinedEvent);
        }
    }

    shareEvent(id?: id) {
        console.log('shareEvent');

        const selectedEventId = id || this.selected.selected;
        const eventIsDefined = typeof selectedEventId !== "undefined";
        if (eventIsDefined) {
            const eventUrl = this.eventsModel.events.find((event) => event.id ===  selectedEventId).eventUrl;
            if (eventUrl) {
                requestShareUrl(eventUrl)
                .then((response) => {
                    this.showConfirmation(ThankYouForSharingTheEvent);
                });
            }
            // we might've been provided by server with falsy link, so no Errors, just no action
            // or may be 'Wrong url to share error message'?
        } else {
            this.showError(AttemptToOperateWithUndefinedEvent);
        }
    }

    backToBrowsing() {
        console.log('backToBrowsing');

        this.selected.selected = undefined;
        this.selected.state = AppState.BROWSE;
        this.fireStateChangedEvent();
    }

    showError(message: string) {
        console.log('showError');

        this.selected.state = AppState.ERROR;
        this.selected.message = message;
        this.fireStateChangedEvent();
    }

    showFriendlyError(message: string) {
        console.log('showFriendlyError');

        this.selected.state = AppState.FRIENDLY_ERROR;
        this.selected.message = message;
        this.fireStateChangedEvent();
    }

    showConfirmation(message: string) {
        console.log('showConfirmation');

        this.selected.state = AppState.SUCCESS;
        this.selected.message = message;
        this.fireStateChangedEvent();
    }

    fireStateChangedEvent() {
        console.log('fireStateChangedEvent');

        document.dispatchEvent(modelChangedEvent());
    }

    rerenderApp() {
        console.log('rerenderApp');

        switch (this.selected.state) {
            case AppState.SUCCESS: {
                this.renderSuccess();
                break;
            }
            case AppState.ERROR: {
                this.renderError();
                break;
            }
            case AppState.FRIENDLY_ERROR: {
                this.renderFriendlyError();
                break;
            }
            case AppState.BROWSE: {
                const eventSelected = typeof this.selected.selected !== "undefined";
                if (eventSelected) {
                    this.renderDetailed();
                } else {
                    this.renderList();
                }
                break;
            }
            default: {
                this.showError(unknownAppState);
                break;
            }
        }
    }
    renderGetPremium() {
        throw new Error("Method not implemented.");
    }

    private renderList() {
        const main = this.emptyAndGet(this.appRoot);
        const events = this.eventsModel.events;
        const listVM: IEventListViewModel = {
            events,
        }
        const {childrenRoot} = new EventList(main, listVM);
        const childrenRootToAppend = this.emptyAndGet(childrenRoot);

        events.forEach((event) => {
            const {id} = event;
            const drillDown = () => this.displayEventDetails(id);
            const onApplyClick = () => this.applyEvent(id);
            const onShareLinkedInClick = () => this.shareEvent(id);

            const vm: IEventViewModel ={
              event,
              drillDown,
              onApplyClick,
              onShareLinkedInClick,
            };
            const newEvent = new SingleEvent(childrenRootToAppend, vm);
        });
    }

    private renderDetailed() {
        const main = this.emptyAndGet(this.appRoot);
        const selectedEvent = this.selected.selected;
        const event = this.eventsModel.events.find((item) => item.id === selectedEvent);

        if (!!event) {
            const vm: IDetailedEventViewModel = {
                event,
                onApplyClick: () => this.applyEvent(event.id),
                onShareLinkedInClick: () => this.shareEvent(event.id),
                backTOBrowsing: this.backToBrowsing,
            }

            const detailed = new DetailedEvent(main, vm);
        }
    }

    private renderError() {
        const main = this.emptyAndGet(this.appRoot);
        const vm: IMessageScreenViewModel = {
            message: this.selected.message,
            backToBrowsing: this.backToBrowsing,
        }
        const error = new ErrorMessage(main, vm);
    }

    private renderSuccess() {
        const main = this.emptyAndGet(this.appRoot);
        const vm: IMessageScreenViewModel = {
            message: this.selected.message,
            backToBrowsing: this.backToBrowsing,
        }
        const confirmation = new Confirmation(main, vm);
    }

    private renderFriendlyError() {
        const main = this.emptyAndGet(this.appRoot);
        const confirmationVm: IMessageScreenViewModel = {
            message: this.selected.message,
            backToBrowsing: this.backToBrowsing,
        }

        const preminmVM: IPremiumLinkViewModel = {
            backToBrowsing: this.backToBrowsing,
            onGetPremiumClick: () => getPremiumNav(),
        }

        const confirmation = new ErrorMessage(main, confirmationVm);
        const premium = new PremiumLink(main, preminmVM);
    }

    private emptyAndGet = (element: JQuery<any>) => {
        element.empty();
        return this.unwrapJQ(element);
    }

    private get appRoot() {
        return $(document).find(getIdSelector(appRoot));
    }

    private get premiumRoot() {
        return $(document).find(getIdSelector(premiumRoot));
    }

    private unwrapJQ = (a: JQuery<HTMLElement>): HTMLElement => {
        return a.get()[0];
    }
}
const unknownAppState = 'UnknownAppState 🤪';
const AttemptToOperateWithUndefinedEvent = 'Attempt To Operate With Undefined Event';
const SomethingWentWrongWithNetwork = 'Something went wrong with network. Please try later.';
const ThisEventOnlyAvailableForPremiumUsers = "Events work great, and the premium events are the 🌟🌟 Best 🌟🌟";
const YouSuccessfulyAppliedTheEvent = "You ✅✅✅ Successfuly ✅✅✅ applied the event";
const ThankYouForSharingTheEvent = "Thank you for sharing the event 🎉🎉🎉";
export const changedEventKey = 'state-changed';
export const modelChangedEvent = () => new Event(changedEventKey);
export enum AppState {
    BROWSE = "BROWSE",
    SUCCESS = "SUCCESS",
    ERROR = "ERROR",
    FRIENDLY_ERROR = "FRIENDLY_ERROR",
}
